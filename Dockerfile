FROM debian:stretch
MAINTAINER big_J

RUN apt-get update && apt install curl gnupg apt-transport-https libasound2 -y
RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg && install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/ && sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
RUN apt-get update && apt-get install code -y # or code-insiders


ENTRYPOINT [ "code" ]
